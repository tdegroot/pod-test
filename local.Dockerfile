# To build the image:
# 1. Launch ssh agent for private key valid for 2 hours: ssh-add -t 7200 ~/.ssh/id_rsa
# 2. Build the image                                   : docker build --ssh default=$SSH_AUTH_SOCK . -t pod-test:v1
#
# To run it: docker run --name pod-test -v /Users/tim/dev/go/web-apps/pod-test/config.yaml:/config.yaml -p 8080:8080 -d pod-test:v1
# To run alpine image interactively:  docker run -it <tag:version> /bin/sh
FROM golang:alpine AS builder

# install necessary software
RUN apk update && apk add --no-cache git openssh-client

# Move to working directory /build
WORKDIR /build

# Set necessary environment variables needed for our image
ENV GO111MODULE=on \
  CGO_ENABLED=0 \
  GOPRIVATE=bitbucket.org/nosboit,bitbucket.org/tdegroot \
  GOOS=linux \
  GOARCH=amd64

# Update git config to use ssh
RUN git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/" 

# Skip Host verification for git
RUN mkdir -p /root/.ssh && \
  chmod 0700 /root/.ssh && \
  ssh-keyscan bitbucket.org > /root/.ssh/known_hosts && \
  chmod 644 /root/.ssh/known_hosts && touch /root/.ssh/config && \
  echo "StrictHostKeyChecking no" > /root/.ssh/config

# Copy and download dependencies using go mod
COPY go.mod .
COPY go.sum .
RUN --mount=type=ssh go mod download

# Copy the code into the container
COPY . .

# Build the application
RUN go build -ldflags "-X main._version=$(git describe --tags)" -o app.bin .

# Move to /dist directory as the place for resulting binary folder
WORKDIR /dist

# Copy binary from build to main folder
RUN cp /build/app.bin .

# Build a small image
FROM scratch

COPY --from=builder /dist/app.bin /

EXPOSE 8080

# Command to run
ENTRYPOINT ["/app.bin"]