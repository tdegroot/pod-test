package main

import "github.com/ilyakaznacheev/cleanenv"

////////////////////
// Config types
///////////////////

type appConfig struct {
	HttpPort string   `yaml:"port"`
	SomeList []string `yaml:"somelist"`
	Secret   string   `yaml:"secret" env:"SECRET"`
}

//////////////////////////////////////
// Read config from file
//////////////////////////////////////

func readConfig(file string) appConfig {
	var cfg appConfig
	err := cleanenv.ReadConfig(file, &cfg)
	panicIf(err)
	return cfg
}
