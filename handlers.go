package main

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
)

// Api status codes for error handling
// 200 - OK
// 400 - Bad Request (Client Error) - A json with error \ more details should return to the client.
// 401 - Unauthorized
// 500 - Internal Server Error - A json with an error should return to the client only when there is no security risk by doing that.

//////////////////////////////////////
// error handlers
//////////////////////////////////////

// func (a application) serverError(c echo.Context, msg string, err error) error {
// 	a.log.Error(msg, err)
// 	return c.JSON(http.StatusInternalServerError, map[string]string{
// 		"error": fmt.Sprintf(msg, err),
// 	})
// }

// func (a application) clientError(c echo.Context, msg string) error {
// 	return c.JSON(http.StatusBadRequest, map[string]string{
// 		"error": msg,
// 	})
// }

//////////////////////////////////////
// route handlers
//////////////////////////////////////

// GET /version
func (a *application) version(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]string{
		"name":    _name,
		"version": _version[1:], // version expected format: v<major>.<minor>.<patch>
	})
}

const html = `<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PodTest</title>
  <!-- <link rel="stylesheet" href="style.css"> -->

  <style>
      body {
      font-family: Tahoma;
      margin-top: 40px;
      margin-left: 50px;
    }

    .grid-container {
      display: grid;
      grid: 20px / 115px 10px 300px;
      grid-gap: 10px;
    }

    .label {
    }

    .text {
      font-family: Monaco;
      color: #4c516d; /* #36454F; */   
    }    
  </style>

</head>
<body>
  <h2>POD Test 😎</h2>
  <div class="grid-container">
    <div>POD local time</div>
    <div>:</div>
    <div class="text">%s</div>
    <div>Some list</div>
    <div>:</div>
    <div class="text">%s</div>
    <div>Secret message</div>
    <div>:</div>
    <div class="text">%s</div>
  </div>
</body>
</html>`

// GET /
func (a *application) test(c echo.Context) error {
	return c.HTML(http.StatusOK, fmt.Sprintf(html,
		time.Now().Format(stdDateTimeFormat),
		strings.Join(a.config.SomeList, "-"),
		a.config.Secret,
	))
}
