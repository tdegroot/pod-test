package main

func (a *application) routes() {
	// static files
	// if a.config.StaticRootDir != "" {
	// 	a.server.Static("/", a.config.StaticRootDir)
	// 	a.log.Info("Serving static files from: %s", a.config.StaticRootDir)
	// }

	// general endpoints
	a.server.GET("/", a.test)
	a.server.GET("/version", a.version)

	// api endpoints
	// a.server.GET("/api/test", a.test)
}
