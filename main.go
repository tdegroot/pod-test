package main

import (
	"context"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/nosboit/simlog"
	"github.com/labstack/echo/v4"
)

func main() {
	var app application

	if fileExists(lookupConfig()) {
		app = application{config: readConfig(lookupConfig())}
	} else {
		app = application{config: appConfig{
			HttpPort: "8080",
			SomeList: []string{"no", "config", "found"},
			Secret:   "Tadaa",
		}}
	}

	app.init()
	app.start()
}

//////////////////////////////////////
// type application
//////////////////////////////////////

type application struct {
	config appConfig
	log    *simlog.Logger
	server *echo.Echo
	quit   chan bool
}

func (a *application) init() {
	a.log = simlog.SimpleConsoleLogger("debug")
	a.server = echo.New()
	a.server.Logger.SetOutput(ioutil.Discard)

	a.quit = make(chan bool)
}

func (a *application) start() {
	defer a.log.Close()
	a.log.Info("Started " + _nameVersion)
	a.log.Info("Loaded config from: %s", lookupConfig())

	// listen for shutdown
	go a.handleShutdownRequest()

	// define routes
	a.routes()

	// CORS
	// if len(a.config.CorsAllowOrigins) > 0 {
	// 	// enable CORS (even a different port on same server is considered a different domain)
	// 	a.server.Use(middleware.CORSWithConfig(middleware.CORSConfig{
	// 		AllowOrigins:     a.config.CorsAllowOrigins,
	// 		AllowCredentials: true,
	// 	}))
	// 	a.log.Info("CORS enabled")
	// }

	// start the HTTP server
	err := a.server.Start(":" + a.config.HttpPort)
	if err != nil && err != http.ErrServerClosed {
		a.log.Fatal(err)
		return
	}

	// wait for HTTP server graceful shutdown to complete
	<-a.quit
}

func (a *application) stop() {
	// gracefully shutdown the http server with a timeout of 7 seconds.
	a.log.Info("Stopping HTTP server")
	ctx, cancel := context.WithTimeout(context.Background(), 7*time.Second)
	defer cancel()

	if err := a.server.Shutdown(ctx); err != nil {
		a.log.Fatal(err)
	}
	a.log.Info("HTTP server stopped")
	a.quit <- true
}

//////////////////////////////////////
// shutdown handler
//////////////////////////////////////

func (a *application) handleShutdownRequest() {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt)

	<-signalChan // block until interrupt received
	// fmt.Print("\b\b")
	a.log.Info("Received shutdown request")
	a.stop()
}
