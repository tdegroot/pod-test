package simlog

import (
	"fmt"
	"os"
)

func panicIf(err error, msg string) {
	if err != nil {
		panic(msg + err.Error())
	}
}

func printIf(err error, action string) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s ERROR: %s: %s\n", DefaultPrefix(), action, err)
	}
}

func isFileAlreadyClosedErr(err error) bool {
	// most os functions that take a path as argument return a *os.PathError
	// which is a struct that can be unpacked for more infofile
	// Here we ignore the file already closed error
	if err != nil {
		if pe, ok := err.(*os.PathError); ok && pe.Err == os.ErrClosed {
			return true
		}
	}
	return false
}

func fileExist(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func deleteFile(path string) {
	err := os.Remove(path)
	if err != nil && !os.IsNotExist(err) {
		panicIf(err, "Failed to delete file "+path)
	}
}
