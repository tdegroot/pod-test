// simlog is a package which provides a Logger through which messages can be logged.
// In order to be able to log a message one or more message writers need to be added
// to the Logger.
// simlog package provides a console and a file message writer.
//
// A message is logged by using a log function which has the following format:
// <Logger>.<severity>(<item>[, <item>]...)
//
// If the log function has only 1 argument, the argument is logged.
// If there are 2 or more arguments, the first argument is considered a Printf
// format string with the remaining args as Printf args
//
// Usage Example:
//
// package main
// import "simlog"
// func main() {
//   log := simlog.NewLogger()
//   log.AddConsoleLogger("debug", simlog.DefaultPrefix)
//   log.Info("This is a log message")
//   log.Error("Logging %d error: %s", 1, "Failed to do whatever")
// }

package simlog

import (
	"fmt"
	"os"
	"sync"
)

////////////////////////////////////////
// Types
////////////////////////////////////////

type logMessage struct {
	level Level
	text  string
}

// Logger is the object through which logging of messages is done
// logging messages with the logger object is thread safe
type Logger struct {
	maxLevel   Level
	msgChannel chan *logMessage
	logWriters []logWriter
	wg         sync.WaitGroup
	once       sync.Once
}

////////////////////////////////////////
// Creating a new logger
////////////////////////////////////////

const logMessageBuffer int = 2000

// NewLogger creates new logger object without any logWriters and starts a worker process.
// LogWriters take care of outputting the message to be logged to the desired
// medium (terminal, file, etc). You need to add these explicitly to the logger.
// The worker process runs in the background and deals with all logMessages send to this logger.
// The created logger is thread safe.
func NewLogger() *Logger {
	lgr := new(Logger)
	lgr.maxLevel = 0
	lgr.msgChannel = make(chan *logMessage, logMessageBuffer)
	lgr.wg.Add(1)
	go lgr.worker()
	return lgr
}

////////////////////////////////////////
// Private methods
////////////////////////////////////////

// worker writes every received log nessage on the channel to the every logwriter
func (lgr *Logger) worker() {
	for msg := range lgr.msgChannel { // for loop ends when channel is closed
		for _, lgw := range lgr.logWriters {
			lgw.writeMessage(msg)
		}
	}

	// closing all logwriters
	for _, lgw := range lgr.logWriters {
		lgw.writeMessage(&logMessage{INFO, "Closing logger"})
		lgw.writer.Close()
	}
	lgr.wg.Done() // signal logWorker has finished
}

////////////////////////////////////////
// Public methods
////////////////////////////////////////

// Close closes the msgChannel and waits until the logworker has finished
func (lgr *Logger) Close() {
	lgr.once.Do(func() {
		close(lgr.msgChannel)
		lgr.wg.Wait() // wait until work finished
	})
}

// Fatal logs the message, closes the logger and then panics with the error message
func (lgr *Logger) Fatal(data ...interface{}) {
	lgr.msgChannel <- &logMessage{FATAL, formatMessage(data...)}
	lgr.Close()
	panic(formatMessage(data...))
}

// FatalIf checks if the supplied error is not nil and if so, calls Fatal
func (lgr *Logger) FatalIf(data ...interface{}) {
	if isError(&data) {
		lgr.Fatal(data...)
	}
}

// Error logs a message with level ERROR
func (lgr *Logger) Error(data ...interface{}) {
	if lgr.maxLevel >= ERROR {
		lgr.msgChannel <- &logMessage{ERROR, formatMessage(data...)}
	}
}

// ErrorIf checks if the supplied error is not nil and if so, calls Error
func (lgr *Logger) ErrorIf(data ...interface{}) {
	if lgr.maxLevel >= ERROR {
		if isError(&data) {
			lgr.Error(data...)
		}
	}
}

// Warn logs a message with level WARN
func (lgr *Logger) Warn(data ...interface{}) {
	if lgr.maxLevel >= WARN {
		lgr.msgChannel <- &logMessage{WARN, formatMessage(data...)}
	}
}

// Info logs a message with level INFO
func (lgr *Logger) Info(data ...interface{}) {
	if lgr.maxLevel >= INFO {
		lgr.msgChannel <- &logMessage{INFO, formatMessage(data...)}
	}
}

// Debug logs a message with level DEBUG
func (lgr *Logger) Debug(data ...interface{}) {
	if lgr.maxLevel >= DEBUG {
		lgr.msgChannel <- &logMessage{DEBUG, formatMessage(data...)}
	}
}

////////////////////////////////////////
// Available loggers
////////////////////////////////////////

// AddConsoleLogger adds a logwriter which prints logmessages to stderr
func (lgr *Logger) AddConsoleLogger(lvlstr string, prefix func() string) {
	lvl := LogLevel(lvlstr)
	lgr.maxLevel = max(lvl, lgr.maxLevel)
	lgr.logWriters = append(lgr.logWriters, logWriter{lvl, prefix, os.Stderr}) // os.Stdout/Stderr has a method Write
}

// AddFileLogger adds a logwriter which writes logmessage to the specified path.
// A .log extension is automatically added to the logfile name except if
// the logfile name already has an .log extension.
// A fileLogger has the following options for logfile rotation:
//
// - day  : rotate per day, a day-of-month nr is added to logfile when shifting to next day.
// - size : rotate on file size and nr of files
//
// If logfile shifting fails, the logger will panic
func (lgr *Logger) AddFileLogger(lvlstr string, prefix func() string, name, dir, polstr string, mxFiles int, mxSize int64) {
	lvl := LogLevel(lvlstr)
	lgr.maxLevel = max(lvl, lgr.maxLevel)
	flgr := newFileLogger(name, dir, RotatePolicy(polstr), mxFiles, mxSize)
	lgr.logWriters = append(lgr.logWriters, logWriter{lvl, prefix, flgr})
}

// AddFileLoggerViaConfig adds a filelogger based on the given LogfileConfig
// see AddFileLogger fro more info
func (lgr *Logger) AddFileLoggerViaConfig(cfg LogfileConfig) {
	lgr.AddFileLogger(cfg.Level, DefaultPrefix, cfg.FileName, cfg.DirName, cfg.RotatePolicy, cfg.MaxFiles, cfg.MaxSizeBytes)
}

////////////////////////////////////////
// Logger helpers
////////////////////////////////////////

// isError is a helper function which checks all parameters for a non nil error.
// If found it returns true, false otherwise.
// error is an interface type with one required method ( Error() string ).
// Since a nil object can't have any methods, the type concersion won't succeed
func isError(data *[]interface{}) bool {
	for _, e := range *data {
		if _, ok := e.(error); ok {
			return true
		}
	}
	return false
}

// formatMessage formats the log message based on the supplied arguments:
// If only one argument is given, log the argument.
// If 2 or more arguments are given, consider 1st arg as a Printf format string
// and the rest of the args as Printf args.
func formatMessage(data ...interface{}) string {
	msg := fmt.Sprint(data...)
	if len(data) > 1 {
		// now we expect first arg to be a format string, we check it to prevent runtime
		// error if it is not
		if f, ok := data[0].(string); ok {
			msg = fmt.Sprintf(f, data[1:]...)
		}
	}
	return msg
}

////////////////////////////////////////
// Covenience functions
////////////////////////////////////////

// SimpleConsoleLogger is convenience function for creating a basic console logger
func SimpleConsoleLogger(lvlstr string) *Logger {
	lgr := NewLogger()
	lgr.AddConsoleLogger(lvlstr, DefaultPrefix)
	return lgr
}

// SimpleFileLogger is convenience function for creating a basic filelogger
// It creates a filelogger with a size based rotate RotatePolicy and will create
// max 3 files.
func SimpleFileLogger(lvlstr string, name, dir string, mxSize int64) *Logger {
	lgr := NewLogger()
	lgr.AddFileLogger(lvlstr, DefaultPrefix, name, dir, "size", 3, mxSize)
	return lgr
}

// FileLoggerViaConfig is convenience function for creating a filelogger
// It creates a file logger with the settings specified in the given config
func FileLoggerViaConfig(cfg LogfileConfig) *Logger {
	lgr := NewLogger()
	lgr.AddFileLogger(cfg.Level, DefaultPrefix, cfg.FileName, cfg.DirName, cfg.RotatePolicy, cfg.MaxFiles, cfg.MaxSizeBytes)
	return lgr
}
