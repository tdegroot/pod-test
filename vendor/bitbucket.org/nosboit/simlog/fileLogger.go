package simlog

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"
)

const logfileExt = ".log"

////////////////////////////////////////
// Types
////////////////////////////////////////

type fileLogger struct {
	baseName     string
	logDir       string
	rotateMode   int
	maxFiles     int
	maxSizeBytes int64
	size         int64
	dayLastWrite int
	file         *os.File
}

////////////////////////////////////////
// Creating a new fileLogger
////////////////////////////////////////

func newFileLogger(name, dir string, mode, mxFiles int, mxSize int64) *fileLogger {
	fl := new(fileLogger)
	fl.baseName = strings.TrimSuffix(name, logfileExt)
	fl.logDir = dir
	fl.rotateMode = mode
	fl.maxFiles = mxFiles
	fl.maxSizeBytes = mxSize

	var err error
	path := filepath.Join(dir, fl.baseName+logfileExt)
	fl.file, err = os.OpenFile(path, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
	panicIf(err, fmt.Sprint("Failed to open file ", path))

	var info os.FileInfo
	info, err = os.Stat(path)
	panicIf(err, fmt.Sprint("Failed to read file ", path))

	fl.size = info.Size()
	fl.dayLastWrite = time.Now().Day()
	return fl
}

////////////////////////////////////////
// Private methods
////////////////////////////////////////

func (fl *fileLogger) logFile(idx int) string {
	name := fmt.Sprintf("%s%s", fl.baseName, logfileExt)
	if idx > 0 {
		name = fmt.Sprintf("%s.%d%s", fl.baseName, idx, logfileExt)
	}
	return filepath.Join(fl.logDir, name)
}

func (fl *fileLogger) shift() {
	var err error

	fl.Close()
	deleteFile(fl.logFile(fl.maxFiles - 1))
	for i := fl.maxFiles; i > 1; i-- {
		if fileExist(fl.logFile(i - 2)) {
			err = os.Rename(fl.logFile(i-2), fl.logFile(i-1))
			panicIf(err, fmt.Sprintf("Failed to rename file %s to %s", fl.logFile(i-2), fl.logFile(i-1)))
		}
	}

	fl.file, err = os.OpenFile(fl.logFile(0), os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
	panicIf(err, fmt.Sprint("Failed to open file ", fl.logFile(0)))

	fl.size = 0
	fl.dayLastWrite = time.Now().Day()
}

////////////////////////////////////////
// Public methods
////////////////////////////////////////

func (fl *fileLogger) Close() error {
	err := fl.file.Close()
	if !isFileAlreadyClosedErr(err) {
		printIf(err, "Closing logfile")
	}
	return nil
}

func (fl *fileLogger) Write(txt []byte) (int, error) {
	switch fl.rotateMode {
	case rotateOnSize:
		if fl.size+int64(len(txt)) > fl.maxSizeBytes {
			fl.shift()
		}
		fl.size += int64(len(txt))
	case rotatePerDay:
		if fl.dayLastWrite != time.Now().Day() {
			fl.shift()
		}
		fl.dayLastWrite = time.Now().Day()
	default:
		panic(fmt.Sprint("Unsupported file rotation mode: ", fl.rotateMode))
	}

	return fl.file.Write(txt)
}

////////////////////////////////////////
// fileLogger helpers
////////////////////////////////////////

const (
	rotateOnSize int = iota
	rotatePerDay
)

// RotatePolicy translates a rotate policy string to a rotate policy constant
func RotatePolicy(pol string) int {
	switch strings.ToUpper(pol) {
	case "DAY":
		return rotatePerDay
	case "SIZE":
		return rotateOnSize
	default:
		return rotatePerDay
	}
}

////////////////////////////////////////
// Convenience functions / types
////////////////////////////////////////

// LogfileConfig is a convenience struct for use by programs which use a file logger
type LogfileConfig struct {
	Level        string
	FileName     string
	DirName      string
	RotatePolicy string
	MaxFiles     int
	MaxSizeBytes int64
}
