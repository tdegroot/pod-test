package simlog

import (
	"fmt"
	"io"
	"time"
)

// DefaultPrefix returns the standard prefix function.
// A log entry has the following format: <prefix> <level>: <msg>
var DefaultPrefix = func() string { return time.Now().Format("2006-01-02 15:04:05") }

type logWriter struct {
	level  Level
	prefix func() string
	writer io.WriteCloser // interface io.WriteCloser defines a Writer and a Closer interface
}

func (lgw logWriter) writeMessage(msg *logMessage) {
	if lgw.level >= msg.level {
		line := fmt.Sprintf("%s %5s: %s\n", lgw.prefix(), msg.level, msg.text)
		_, err := lgw.writer.Write([]byte(line))
		printIf(err, "Writing log message")
	}
}
