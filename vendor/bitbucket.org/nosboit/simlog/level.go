package simlog

import (
	"strings"
)

// Level is the log level of the message to be logged
type Level int

// Though the constants naming standard is to use camelcase
// I chose capitals here to avoid confusion with error type
const (
	FATAL Level = iota
	ERROR
	WARN
	INFO
	DEBUG
)

var levels = [...]string{"FATAL", "ERROR", "WARN", "INFO", "DEBUG"}

// String values for log level constants
func (l Level) String() string {
	return levels[l]
}

// LogLevel returns the loglevel type corresponding with the supplied loglevel string value (case insensitive)
// If no corresponding level is found, level DEBUG is returned
func LogLevel(lvl string) Level {
	for i, v := range levels {
		if v == strings.ToUpper(lvl) {
			return Level(i)
		}
	}
	return Level(DEBUG)
}

// max is a helper function used for comparing 2 loglevels
func max(x, y Level) Level {
	if x > y {
		return x
	}
	return y
}
