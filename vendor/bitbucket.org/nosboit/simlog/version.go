package simlog

const (
	_version     = "1.2.1"
	_name        = "Simple logger library"
	_nameVersion = _name + " " + _version
)

// Version returns the name and version of this library
func Version() string {
	return _nameVersion
}
