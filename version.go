package main

var (
	_version     string = "v0.0.0" // will be automatically set to current git tag at compile time
	_name        string = "PODTest"
	_nameVersion string = _name + " " + _version
)
