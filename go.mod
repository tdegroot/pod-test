module bitbucket.org/tdegroot/pod-test

go 1.16

require (
	bitbucket.org/nosboit/simlog v1.2.1
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/ilyakaznacheev/cleanenv v1.2.5
	github.com/labstack/echo/v4 v4.5.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
